﻿using System;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.Resources;
using Windows.Globalization;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using KarambaPlanning.App1.Messages;
using KarambaPlanning.App1.ViewModel;
using KarambaPlanning.App1.Views;

namespace KarambaPlanning.App1
{
  /// <summary>
  ///   Provides application-specific behavior to supplement the default Application class.
  /// </summary>
  sealed partial class App : Application
  {
    private const int SettingsPaneWidth = 400;
    private readonly IMessenger _messenger;
    private readonly ResourceLoader _resourceLoader = new ResourceLoader();
    private Popup _settingsPopup;

    /// <summary>
    ///   Initializes the singleton application object.  This is the first line of authored code
    ///   executed, and as such is the logical equivalent of main() or WinMain().
    /// </summary>
    public App()
    {
      InitializeComponent();
      RegisterRemainder();
      Suspending += OnSuspending;

      _messenger = (IMessenger) ViewModelLocator.Container.GetInstance(typeof (IMessenger), null);
    }

    private void RegisterRemainder()
    {
      const string taskName = "RemainderTask";
      var registered = BackgroundTaskRegistration.AllTasks.Any(t => t.Value.Name == taskName);

      if (!registered)
      {
        var builder = new BackgroundTaskBuilder
        {
          Name = taskName,
          TaskEntryPoint = "KarambaPlanning.BackgroundAgent.RemainderBackgroundTask"
        };

        IBackgroundTrigger trigger = new MaintenanceTrigger(15, oneShot: false);
        builder.SetTrigger(trigger);
        builder.Register();
      }
    }

    /// <summary>
    ///   Invoked when the application is launched normally by the end user.  Other entry points
    ///   will be used such as when the application is launched to open a specific file.
    /// </summary>
    /// <param name="e">Details about the launch request and process.</param>
    protected override void OnLaunched(LaunchActivatedEventArgs e)
    {
      //#if DEBUG
      //      if (Debugger.IsAttached) 
      //      {
      //        DebugSettings.EnableFrameRateCounter = true;
      //      }
      //#endif

      var settingsPane = SettingsPane.GetForCurrentView();
      settingsPane.CommandsRequested += SettingsPaneOnCommandsRequested;

      var rootFrame = Window.Current.Content as Frame;

      // Do not repeat app initialization when the Window already has content,
      // just ensure that the window is active
      if (rootFrame == null)
      {
        // Create a Frame to act as the navigation context and navigate to the first page
        rootFrame = new Frame();
        // Set the default language
        rootFrame.Language = ApplicationLanguages.Languages[0];

        rootFrame.NavigationFailed += OnNavigationFailed;

        if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
        {
          //TODO: Load state from previously suspended application
        }

        // Place the frame in the current Window
        Window.Current.Content = rootFrame;
        DispatcherHelper.Initialize();
      }

      if (rootFrame.Content == null)
      {
        rootFrame.Navigate(typeof (MonthSheetPage), e.Arguments);
      }

      if (e.Arguments.Contains("toast"))
      {
        var message = new InitDayPageMessage(DateTime.Today);
        _messenger.Send(message);

        rootFrame.Navigate(typeof (ThreeDaysSheetPage), e.Arguments);
      }

      // Ensure the current window is active
      Window.Current.Activate();
    }

    private void SettingsPaneOnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
    {
      var applicationCommands = args.Request.ApplicationCommands;

      var newComand = new SettingsCommand("common", _resourceLoader.GetString("Common"), cmd =>
      {
        _settingsPopup = new Popup
        {
          IsLightDismissEnabled = true,
          Width = SettingsPaneWidth,
          Height = Window.Current.Bounds.Height,
          ChildTransitions = new TransitionCollection
          {
            new PaneThemeTransition
            {
              Edge = (SettingsPane.Edge == SettingsEdgeLocation.Right) ?
                       EdgeTransitionLocation.Right : EdgeTransitionLocation.Left
            }
          }
        };

        var mypane = new SettingsFlyout1
        {
          Width = SettingsPaneWidth,
          Height = Window.Current.Bounds.Height
        };
        _settingsPopup.Child = mypane;
        _settingsPopup.SetValue(Canvas.LeftProperty, SettingsPane.Edge == SettingsEdgeLocation.Right ?
                                                       Window.Current.Bounds.Width - SettingsPaneWidth : 0);
        _settingsPopup.SetValue(Canvas.TopProperty, 0);
        _settingsPopup.IsOpen = true;
      });

      applicationCommands.Add(newComand);
    }

    /// <summary>
    ///   Invoked when Navigation to a certain page fails
    /// </summary>
    /// <param name="sender">The Frame which failed navigation</param>
    /// <param name="e">Details about the navigation failure</param>
    private void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
    {
      throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
    }

    /// <summary>
    ///   Invoked when application execution is being suspended.  Application state is saved
    ///   without knowing whether the application will be terminated or resumed with the contents
    ///   of memory still intact.
    /// </summary>
    /// <param name="sender">The source of the suspend request.</param>
    /// <param name="e">Details about the suspend request.</param>
    private void OnSuspending(object sender, SuspendingEventArgs e)
    {
//      var settingsPane = SettingsPane.GetForCurrentView();
//      settingsPane.CommandsRequested -= SettingsPaneOnCommandsRequested;

      var deferral = e.SuspendingOperation.GetDeferral();
      //TODO: Save application state and stop any background activity
      deferral.Complete();
    }
  }
}