﻿using System;
using Windows.UI.Xaml.Data;

namespace KarambaPlanning.App1.Converters
{
  public class BoolNegateConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, string language)
    {
      return !(bool) value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, string language)
    {
      throw new NotImplementedException();
    }
  }
}