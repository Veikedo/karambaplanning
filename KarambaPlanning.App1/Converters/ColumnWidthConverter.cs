﻿using System;
using Windows.UI.Xaml.Data;

namespace KarambaPlanning.App1.Converters
{
  public class ColumnWidthConverter : IValueConverter
  {
    private const int ColumnCount = 7;

    public object Convert(object value, Type targetType, object parameter, string language)
    {
      double width = System.Convert.ToDouble(value);
      return width/ColumnCount;
    }

    public object ConvertBack(object value, Type targetType, object parameter, string language)
    {
      throw new NotImplementedException();
    }
  }
}