﻿using System;
using Windows.UI.Xaml.Data;

namespace KarambaPlanning.App1.Converters
{
  public class DateTimeToDateTimeOffsetConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, string language)
    {
      var dateTime = (DateTime) value;
      return new DateTimeOffset(dateTime);
    }

    public object ConvertBack(object value, Type targetType, object parameter, string language)
    {
      return ((DateTimeOffset) value).DateTime;
    }
  }
}