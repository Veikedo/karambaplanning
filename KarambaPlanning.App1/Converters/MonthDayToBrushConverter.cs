﻿using System;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using KarambaPlanning.App1.ViewModel.Spans;

namespace KarambaPlanning.App1.Converters
{
  public class MonthDayToBrushConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, string language)
    {
      var model = (MonthDaySpanViewModel) value;
      Color color;

      if (model.Date == DateTime.Today)
      {
        color = Color.FromArgb(255, 160, 160, 200);
      }
      else if (model.IsCurrentMonthAffix)
      {
        color = Color.FromArgb(255, 224, 223, 226);
      }
      else
      {
        color = Color.FromArgb(255, 232, 231, 234);
      }

      return new SolidColorBrush(color);
    }

    public object ConvertBack(object value, Type targetType, object parameter, string language)
    {
      throw new NotImplementedException();
    }
  }
}