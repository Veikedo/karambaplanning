﻿using System;
using Windows.UI.Xaml.Data;

namespace KarambaPlanning.App1.Converters
{
  public class RowHeightConverter : IValueConverter
  {
    private const int RowCount = 6;

    public object Convert(object value, Type targetType, object parameter, string language)
    {
      double height = System.Convert.ToDouble(value);
      double surplus = System.Convert.ToDouble(parameter);

      return (height - surplus)/RowCount;
    }

    public object ConvertBack(object value, Type targetType, object parameter, string language)
    {
      throw new NotImplementedException();
    }
  }
}