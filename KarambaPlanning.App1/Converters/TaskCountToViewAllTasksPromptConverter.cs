﻿using System;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Data;

namespace KarambaPlanning.App1.Converters
{
  public class TaskCountToViewAllTasksPromptConverter : IValueConverter
  {
    private readonly ResourceLoader _resources = new ResourceLoader();
    private const int TasksInSpan = 3;

    public object Convert(object value, Type targetType, object parameter, string language)
    {
      int taskCount = System.Convert.ToInt32(value);
      if (taskCount > TasksInSpan)
      {
        return string.Format("+{0} {1}", taskCount - TasksInSpan, _resources.GetString("More"));
      }

      return string.Empty;
    }

    public object ConvertBack(object value, Type targetType, object parameter, string language)
    {
      throw new NotImplementedException();
    }
  }
}