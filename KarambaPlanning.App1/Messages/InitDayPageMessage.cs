﻿using System;
using GalaSoft.MvvmLight.Messaging;

namespace KarambaPlanning.App1.Messages
{
  /// <summary>
  /// Indicates that day page should set specific date
  /// </summary>
  public class InitDayPageMessage : MessageBase
  {
    public InitDayPageMessage(DateTime date)
    {
      Date = date;
    }

    public DateTime Date { get; private set; }
  }
}