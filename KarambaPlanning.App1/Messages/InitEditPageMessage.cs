﻿using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.ViewModel;

namespace KarambaPlanning.App1.Messages
{
  /// <summary>
  /// Indicates that edit page should set specific taskViewModel
  /// </summary>
  public class InitEditPageMessage : MessageBase
  {
    public InitEditPageMessage(TaskViewModel model, bool isNewTask)
    {
      Model = model;
      IsNewTask = isNewTask;
    }

    public TaskViewModel Model { get; set; }
    public bool IsNewTask { get; set; }
  }
}