﻿using System;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.Messages;
using KarambaPlanning.App1.Views;

namespace KarambaPlanning.App1.ViewModel
{
  /// <summary>
  /// Base class for all view models in project
  /// </summary>
  public class BaseViewModel : ViewModelBase
  {
    protected readonly INavigationService NavigationService;

    protected BaseViewModel(IMessenger messenger, INavigationService navigationService) : base(messenger)
    {
      NavigationService = navigationService;
    }

    protected void NavigateToDayPage(DateTime date)
    {
      var message = new InitDayPageMessage(date);

      MessengerInstance.Send(message);
      NavigationService.Navigate(typeof(ThreeDaysSheetPage));
    }

    protected void NavigateToTaskEditPage(TaskViewModel model)
    {
      var message = new InitEditPageMessage(model, isNewTask: model.Id == 0);

      MessengerInstance.Send(message);
      NavigationService.Navigate(typeof (EditPage));
    }
  }
}