﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Windows.ApplicationModel.Resources;
using Windows.Storage;
using GalaSoft.MvvmLight;
using KarambaPlanning.App1.Common;
using KarambaPlanning.Repository.Contracts;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace KarambaPlanning.App1.ViewModel.ContractViewModels
{
  /// <summary>
  ///   ViewModel for a settings pane
  /// </summary>
  public class SettingsPaneViewModel : ViewModelBase
  {
    private readonly INavigationService _navigationService;
    private readonly IRepository _repository;

    private ObservableCollection<RemainderData> _remainderOptions;
    private RemainderData _selectedRemainder;

    public SettingsPaneViewModel(IRepository repository, INavigationService navigationService)
    {
      _repository = repository;
      _navigationService = navigationService;
      Init();
    }

    /// <summary>
    ///   Available positions for reminder
    /// </summary>
    public ObservableCollection<RemainderData> RemainderOptions
    {
      get { return _remainderOptions; }
      set { Set(ref _remainderOptions, value); }
    }

    /// <summary>
    ///   Current remainder
    /// </summary>
    public RemainderData SelectedRemainder
    {
      get { return _selectedRemainder; }
      set { Set(ref _selectedRemainder, value); }
    }

    /// <summary>
    ///   Changes current remainder
    /// </summary>
    public ICommand UpdateRemainderCommand
    {
      get { return new RelayCommand(UpdateRemainder); }
    }

    /// <summary>
    /// Deletes all tasks from a database
    /// </summary>
    public ICommand DeleteAllTasksCommand
    {
      get { return new RelayCommand(DeleteAllTasks); }
    }

    private async void DeleteAllTasks()
    {
      await _repository.RemoveAllDateTasksAsync();

      var currentPageType = _navigationService.CurrentPageType;
      _navigationService.Navigate(currentPageType);
    }

    private void Init()
    {
      var resourceLoader = new ResourceLoader();

      string hour = resourceLoader.GetString("Hour");
      string hours = resourceLoader.GetString("Hours");
      string minutes = resourceLoader.GetString("Minutes");

      RemainderOptions = new ObservableCollection<RemainderData>
      {
        new RemainderData("30 " + minutes, TimeSpan.FromMinutes(30).Ticks),
        new RemainderData("1 " + hour, TimeSpan.FromHours(1).Ticks),
        new RemainderData("2 " + hours, TimeSpan.FromHours(2).Ticks),
      };

      object currentRemainder;
      if (ApplicationData.Current.LocalSettings.Values.TryGetValue("Remainder", out currentRemainder))
      {
        var ticks = (long) currentRemainder;
        SelectedRemainder = RemainderOptions.First(x => x.Ticks == ticks);
      }
      else
      {
        SelectedRemainder = RemainderOptions.First();
        UpdateRemainder();
      }
    }

    private void UpdateRemainder()
    {
      ApplicationData.Current.LocalSettings.Values["Remainder"] = SelectedRemainder.Ticks;
    }


    /// <summary>
    /// POCO for storing remainder positions
    /// </summary>
    public class RemainderData
    {
      public RemainderData(string key, long ticks)
      {
        Key = key;
        Ticks = ticks;
      }

      public string Key { get; private set; }
      public long Ticks { get; private set; }
    }
  }
}