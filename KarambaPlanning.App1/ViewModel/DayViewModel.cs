﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.ViewModel.Spans;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace KarambaPlanning.App1.ViewModel
{
  /// <summary>
  /// Model for a day representer in Week and ThreeDays
  /// </summary>
  public class DayViewModel : BaseViewModel
  {
    public DayViewModel(IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
      InterimTasks = new List<InterimTaskSpan>();
    }

    /// <summary>
    /// Header of a group
    /// </summary>
    public string DateHeader { get; set; }

    /// <summary>
    /// Date for a tasks in a group
    /// </summary>
    public DateTime Date { get; set; }

    /// <summary>
    /// Span for the day tasks
    /// </summary>
    public SheetSpanViewModel DayTasksSpan { get; set; }

    /// <summary>
    /// Spans for a interim tasks
    /// </summary>
    public IList<InterimTaskSpan> InterimTasks { get; set; }

    /// <summary>
    /// Command that called when click on DayTasksSpan
    /// </summary>
    public ICommand CreateNewTaskCommand
    {
      get
      {
        return new RelayCommand(() => NavigateToTaskEditPage(new TaskViewModel(MessengerInstance, NavigationService)
        {
          Date = Date,
          IsAllDayTask = true
        }));
      }
    }
  }
}