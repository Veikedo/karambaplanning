﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace KarambaPlanning.App1.ViewModel
{
  /// <summary>
  /// POCO for storing data in GroupView
  /// </summary>
  /// <typeparam name="TKey"></typeparam>
  /// <typeparam name="TItem"></typeparam>
  public class GroupViewModel<TKey, TItem> : ViewModelBase
  {
    private ObservableCollection<TItem> _items;
    private TKey _key;

    public GroupViewModel()
    {
      _items = new ObservableCollection<TItem>();
    }

    /// <summary>
    /// Key (header) of group
    /// </summary>
    public TKey Key
    {
      get { return _key; }
      set { Set(ref _key, value); }
    }


    /// <summary>
    /// Items in a group
    /// </summary>
    public ObservableCollection<TItem> Items
    {
      get { return _items; }
      set { Set(ref _items, value); }
    }
  }
}