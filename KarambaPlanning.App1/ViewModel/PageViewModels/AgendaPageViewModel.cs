﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.ViewModel.Spans;
using KarambaPlanning.Helpers;
using KarambaPlanning.Repository;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.ViewModel.PageViewModels
{
  /// <summary>
  /// Base class for views with a day agenda
  /// </summary>
  public abstract class AgendaPageViewModel : SheetViewModel
  {
    private readonly IRepository _repository;
    private ObservableCollection<DayViewModel> _sheetGroups;

    protected AgendaPageViewModel(IRepository repository, IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
      _repository = repository;
    }

    /// <summary>
    /// Storing day viewModels
    /// </summary>
    public ObservableCollection<DayViewModel> SheetGroups
    {
      get { return _sheetGroups; }
      set { Set(ref _sheetGroups, value); }
    }

    /// <summary>
    /// Count of displayed days in the view
    /// </summary>
    protected abstract int DayCount { get; }

    protected sealed override async Task UpdateView(CancellationToken ct)
    {
      if (ct.IsCancellationRequested)
      {
        return;
      }
      
      await UpdateSheet(ct);
      if (!ct.IsCancellationRequested)
      {
        PageTitle = CreatePageTitle(SheetGroups.First().Date, SheetGroups.Last().Date);
      }
    }

    private string CreatePageTitle(DateTime startDate, DateTime endDate)
    {
      var sb = new StringBuilder();
      sb.AppendFormat("{0} {1}", Culture.GetMonthName(startDate.Month), startDate.Year);

      if (startDate.Month != endDate.Month)
      {
        sb.AppendFormat(" - {0} {1}", Culture.GetMonthName(endDate.Month), endDate.Year);
      }

      return sb.ToString();
    }

    private async Task UpdateSheet(CancellationToken ct)
    {
      var newSheet = new ObservableCollection<DayViewModel>();

      var date = SheetDate;
      for (int i = 0; i < DayCount; i++)
      {
        if (ct.IsCancellationRequested)
        {
          return;
        }

        var model = await CreateDayModelAsync(date);
        newSheet.Add(model);

        date = date.AddDays(1);
      }

      if (!ct.IsCancellationRequested)
      {
        SheetGroups = newSheet;
      }
    }

    private async Task<DayViewModel> CreateDayModelAsync(DateTime date)
    {
      // ReSharper disable PossibleMultipleEnumeration
      var dateTasks = (await _repository.GetDateTasksAsync(x => x.Date == date));

      var allDayTasks = dateTasks.Where(t => t.IsAllDayTask);
      var retValue = new DayViewModel(MessengerInstance, NavigationService)
      {
        Date = date,
        DateHeader = date.ToString("ddd, dd"),
        DayTasksSpan = new SheetSpanViewModel(MessengerInstance, NavigationService)
        {
          Date = date,
          Tasks = FilterDateTasks(allDayTasks).Select(BuildTaskViewModel),
          RealTaskCount = allDayTasks.Count()
        }
      };

      var interimTasks = dateTasks.Where(t => !t.IsAllDayTask);

      var startTime = new TimeSpan();
      while (startTime.Days == 0)
      {
        TimeSpan endTime = startTime.AddHours(1);
        var interimTasksInSpan = FindTasksInSpan(interimTasks, startTime, endTime);

        var model = new InterimTaskSpan(MessengerInstance, NavigationService)
        {
          Date = date,
          StartTime = startTime,
          EndTime = endTime,
          Tasks = FilterDateTasks(interimTasksInSpan).Select(BuildTaskViewModel),
          RealTaskCount = interimTasksInSpan.Count()
        };

        retValue.InterimTasks.Add(model);
        startTime = endTime;
      }

      return retValue;
      // ReSharper restore PossibleMultipleEnumeration
    }

    private IEnumerable<DateTask> FindTasksInSpan(IEnumerable<DateTask> interimTasks,
                                                  TimeSpan start, TimeSpan end)
    {
      long startTicks = start.Ticks;
      long endTicks = end.Ticks;

      var retValue = interimTasks.Where(x => x.StartTime >= startTicks && x.StartTime < endTicks)
                                 .OrderBy(x => x.StartTime);

      return retValue;
    }

    protected virtual IEnumerable<DateTask> FilterDateTasks(IEnumerable<DateTask> dateTasks)
    {
      return dateTasks;
    }
  }
}