﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.ViewModel.Spans;
using KarambaPlanning.Helpers;
using KarambaPlanning.Repository;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.ViewModel.PageViewModels
{
  /// <summary>
  /// ViewModel for a month view
  /// </summary>
  public class MonthPageViewModel : SheetViewModel
  {
    #region Fields

    private const int MaxTasksInContainer = 3;
    private readonly DayOfWeekComparer _dayOfWeekComparer;
    private readonly IRepository _repository;
    private ObservableCollection<GroupViewModel<string, MonthDaySpanViewModel>> _sheetGroups;

    #endregion

    public MonthPageViewModel(IRepository repository, IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
      _repository = repository;
      _dayOfWeekComparer = new DayOfWeekComparer();
    }

    /// <summary>
    /// Storing day viewModels
    /// </summary>
    public ObservableCollection<GroupViewModel<string, MonthDaySpanViewModel>> SheetGroups
    {
      get { return _sheetGroups; }
      set { Set(ref _sheetGroups, value); }
    }

    protected override void FlipSheetBack()
    {
      SheetDate = SheetDate.AddMonths(-1);
    }

    protected override void FlipSheetForward()
    {
      SheetDate = SheetDate.AddMonths(1);
    }

    protected override async Task UpdateView(CancellationToken ct)
    {
      if (ct.IsCancellationRequested)
      {
        return;
      }

      PageTitle = string.Format("{0} {1}", Culture.GetMonthName(SheetDate.Month), SheetDate.Year);
      await UpdateSheet(ct);
    }

    protected override void SetInitialDate()
    {
      SheetDate = DateTime.Today;
    }

    private async Task UpdateSheet(CancellationToken ct)
    {
      SheetGroups = new ObservableCollection<GroupViewModel<string, MonthDaySpanViewModel>>();

      DayOfWeek day = Culture.FirstDayOfWeek;
      for (int i = 0; i < 7; i++)
      {
        if (ct.IsCancellationRequested)
        {
          return;
        }

        var model = await CreateMonthGroupAsync(SheetDate.Year, SheetDate.Month, day);

        if (ct.IsCancellationRequested)
        {
          return;
        }

        SheetGroups.Add(model);
        day = (DayOfWeek) ((int) (day + 1)%7);
      }

    }

    private async Task<GroupViewModel<string, MonthDaySpanViewModel>> CreateMonthGroupAsync(int year, int month,
                                                                                        DayOfWeek dayOfWeek)
    {
      var res = new GroupViewModel<string, MonthDaySpanViewModel>
      {
        Key = Culture.GetAbbreviatedDayName(dayOfWeek)
      };

      DateTime groupDay = new DateTime(year, month, 1).FirstDayOfWeekOfMonth(dayOfWeek);
      int groupCounter = 0;

      // add affix from previous month
      if (_dayOfWeekComparer.Compare(groupDay.DayOfWeek, groupDay.FirstDayOfMonth().DayOfWeek) < 0)
      {
        DateTime lastDay = groupDay.LastDayOfWeek(dayOfWeek);
        var model = new MonthDaySpanViewModel(MessengerInstance, NavigationService)
        {
          Date = lastDay,
          IsCurrentMonthAffix = true,
          Tasks = await GetDayViewModelsAsync(lastDay),
          RealTaskCount = await _repository.CountAsync(t => t.Date == lastDay)
        };

        res.Items.Add(model);
        groupCounter++;
      }

      while (groupCounter < 6)
      {
        DateTime day = groupDay;

        var model = new MonthDaySpanViewModel(MessengerInstance, NavigationService)
        {
          Date = groupDay,
          IsCurrentMonthAffix = groupDay.Month != month,
          Tasks = await GetDayViewModelsAsync(groupDay),
          RealTaskCount = await _repository.CountAsync(t => t.Date == day)
        };

        res.Items.Add(model);
        
        groupDay = groupDay.NextDayOfWeek(dayOfWeek);
        groupCounter++;
      }

      return res;
    }

    private async Task<IEnumerable<TaskViewModel>> GetDayViewModelsAsync(DateTime date)
    {
      IEnumerable<DateTask> tasks = await _repository.GetDateTasksAsync(x => x.Date == date);

      var res = tasks.OrderByDescending(t => t.IsAllDayTask)
                     .ThenBy(t => t.StartTime)
                     .Take(MaxTasksInContainer)
                     .Select(BuildTaskViewModel);

      return await Task.FromResult(res);
    }
  }
}