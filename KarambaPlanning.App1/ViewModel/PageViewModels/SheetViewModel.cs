﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.Views;
using KarambaPlanning.Repository;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace KarambaPlanning.App1.ViewModel.PageViewModels
{
  /// <summary>
  ///   Base class for the calendar viewModels
  /// </summary>
  public abstract class SheetViewModel : BaseViewModel
  {
    private CancellationTokenSource _cts;
    private string _pageTitle;
    private DateTime _sheetDate;

    protected SheetViewModel(IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
      _cts = new CancellationTokenSource();
      SetInitialDate();
    }

    /// <summary>
    ///   Initiate viewModel when view has opened (? sorry for my French)
    /// </summary>
    public ICommand LoadedCommand
    {
      get { return new RelayCommand(RunUpdateView); }
    }

    /// <summary>
    ///   Handles jump to next calendar sheet
    /// </summary>
    public ICommand FlipSheetForwardCommand
    {
      get
      {
        return new RelayCommand(() =>
        {
          FlipSheetForward();
          RunUpdateView();
        });
      }
    }

    /// <summary>
    ///   Handles jump to previous calendar sheet
    /// </summary>
    public ICommand FlipSheetBackCommand
    {
      get
      {
        return new RelayCommand(() =>
        {
          FlipSheetBack();
          RunUpdateView();
        });
      }
    }

    /// <summary>
    ///   Handles jump to the week view
    /// </summary>
    public ICommand OpenWeekPageCommand
    {
      get { return new Common.RelayCommand(() => NavigationService.Navigate(typeof (WeekSheetPage))); }
    }

    /// <summary>
    ///   Handles jump to the month view
    /// </summary>
    public ICommand OpenMonthPageCommand
    {
      get { return new Common.RelayCommand(() => NavigationService.Navigate(typeof (MonthSheetPage))); }
    }

    /// <summary>
    ///   Handles jump to the day view
    /// </summary>
    public ICommand OpenDayPageCommand
    {
      get { return new RelayCommand(() => NavigationService.Navigate(typeof (ThreeDaysSheetPage))); }
    }

    /// <summary>
    ///   Jumps to the current date
    /// </summary>
    public ICommand SyncToCurrentDateCommand
    {
      get
      {
        return new RelayCommand(() =>
        {
          SetInitialDate();
          RunUpdateView();
        });
      }
    }

    /// <summary>
    ///   Title of the page
    /// </summary>
    public string PageTitle
    {
      get { return _pageTitle; }
      set { Set(ref _pageTitle, value); }
    }

    public DateTime SheetDate
    {
      get { return _sheetDate; }
      set { Set(ref _sheetDate, value); }
    }

    protected static DateTimeFormatInfo Culture
    {
      get { return DateTimeFormatInfo.CurrentInfo; }
    }

    protected abstract void FlipSheetBack();

    protected abstract void FlipSheetForward();

    protected TaskViewModel BuildTaskViewModel(DateTask task)
    {
      return new TaskViewModel(MessengerInstance, NavigationService)
      {
        Id = task.Id,
        Date = task.Date,
        Subject = task.Subject,
        Message = task.Message,
        Location = task.Location,
        IsAllDayTask = task.IsAllDayTask,
        StartTime = TimeSpan.FromTicks(task.StartTime),
        EndTime = TimeSpan.FromTicks(task.EndTime)
      };
    }

    private async void RunUpdateView()
    {
      _cts.Cancel();
      _cts = new CancellationTokenSource();

      await UpdateView(_cts.Token);
    }

    protected abstract Task UpdateView(CancellationToken token);
    protected abstract void SetInitialDate();
  }
}