﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Windows.ApplicationModel.Resources;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.Messages;
using KarambaPlanning.Helpers;
using KarambaPlanning.Repository;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.ViewModel.PageViewModels
{
  /// <summary>
  /// ViewModel for a task edit view
  /// </summary>
  public class TaskEditPageViewModel : BaseViewModel
  {
    #region Fields

    private readonly Dictionary<string, string> _modelErrors;
    private readonly IRepository _repository;
    private readonly ResourceLoader _resources;
    private DateTime _date;
    private ObservableCollection<string> _errors;
    private TimeSpan _endTime;
    private bool _isAllDayTask;
    private bool _isNewtask;
    private string _location;
    private string _message;
    private string _pageTitle;
    private TimeSpan _startTime;
    private string _subject;
    private int _taskId;

    #endregion

    public TaskEditPageViewModel(IMessenger messenger, INavigationService navigationService, IRepository repository)
      : base(messenger, navigationService)
    {
      _repository = repository;
      _errors = new ObservableCollection<string>();
      _resources = new ResourceLoader();
      _modelErrors = new Dictionary<string, string>();

      MessengerInstance.Register(this, (Action<InitEditPageMessage>) InitPageMessageReceived);
    }

    /// <summary>
    /// Titile of the page
    /// </summary>
    public string PageTitle
    {
      get { return _pageTitle; }
      set { Set(ref _pageTitle, value); }
    }

    /// <summary>
    /// Date of the task
    /// </summary>
    public DateTime Date
    {
      get { return _date; }
      set { Set(ref _date, value.Date); }
    }


    /// <summary>
    /// Is task created for all day
    /// </summary>
    public bool IsAllDayTask
    {
      get { return _isAllDayTask; }
      set { Set(ref _isAllDayTask, value); }
    }

    /// <summary>
    /// Time of start
    /// </summary>
    public TimeSpan StartTime
    {
      get { return _startTime; }
      set { Set(ref _startTime, value); }
    }


    /// <summary>
    /// Time of end
    /// </summary>
    public TimeSpan EndTime
    {
      get { return _endTime; }
      set { Set(ref _endTime, value); }
    }

    /// <summary>
    /// Location of the task
    /// </summary>
    public string Location
    {
      get { return _location; }
      set { Set(ref _location, value); }
    }


    /// <summary>
    /// Subject of the task
    /// </summary>
    public string Subject
    {
      get { return _subject; }
      set { Set(ref _subject, value); }
    }

    /// <summary>
    /// Subject of the task
    /// </summary>
    public string Message
    {
      get { return _message; }
      set { Set(ref _message, value); }
    }

    /// <summary>
    /// If the task is not saved yet we can't delete task
    /// </summary>
    public bool CanDeleteTask
    {
      get { return !_isNewtask; }
    }

    /// <summary>
    /// Errors in the model
    /// </summary>
    public ObservableCollection<string> Errors
    {
      get { return _errors; }
      set { Set(ref _errors, value); }
    }

    /// <summary>
    /// Deletes task from a database
    /// </summary>
    public ICommand DeleteTaskCommand
    {
      get { return new RelayCommand(DeleteTaskAsync); }
    }

    /// <summary>
    /// Saves task to a database
    /// </summary>
    public ICommand SaveCommand
    {
      get { return new RelayCommand(SaveAsync); }
    }

    private async void DeleteTaskAsync()
    {
      await _repository.RemoveDateTaskAsync(_taskId);
      NavigationService.GoBack();
    }

    private async void SaveAsync()
    {
      if (!ValidateModel())
      {
        Errors = new ObservableCollection<string>(_modelErrors.Values);
        return;
      }

      var task = new DateTask
      {
        Id = _taskId,
        Subject = Subject,
        Message = Message,
        Location = Location,
        Date = Date,
        IsAllDayTask = IsAllDayTask,
        StartTime = StartTime.Ticks,
        EndTime = EndTime.Ticks
      };

      if (_isNewtask)
      {
        await _repository.CreateDateTaskAsync(task);
      }
      else
      {
        await _repository.UpdateDateTaskAsync(task);
      }

      NavigationService.GoBack();
    }

    private bool ValidateModel()
    {
      if (_subject.IsNullOrEmpty())
      {
        _modelErrors["Subject"] = _resources.GetString("SubjectIncorrect");
      }
      else
      {
        _modelErrors.Remove("Subject");
      }

      if (!IsAllDayTask && EndTime <= StartTime)
      {
        _modelErrors["Time"] = _resources.GetString("EndTimeIncorrect");
      }
      else
      {
        _modelErrors.Remove("Time");
      }

      return _modelErrors.Count == 0;
    }

    private void InitPageMessageReceived(InitEditPageMessage m)
    {
      var loader = new ResourceLoader();
      TaskViewModel model = m.Model;

      Date = model.Date;
      PageTitle = m.IsNewTask ? loader.GetString("AddTask") : loader.GetString("EditTask");
      IsAllDayTask = model.IsAllDayTask;
      StartTime = model.StartTime;
      EndTime = model.EndTime >= model.StartTime ? model.StartTime.AddHours(1) : model.EndTime;
      Location = model.Location;
      Subject = model.Subject;
      Message = model.Message;
      _taskId = model.Id;
      _isNewtask = m.IsNewTask;

      _modelErrors.Clear();
      Errors.Clear();
    }
  }
}