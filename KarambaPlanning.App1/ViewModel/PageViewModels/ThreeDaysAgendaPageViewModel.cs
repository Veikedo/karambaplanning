using System;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.Messages;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.ViewModel.PageViewModels
{
  /// <summary>
  ///   ViewModel for a day view
  /// </summary>
  public class ThreeDaysAgendaPageViewModel : AgendaPageViewModel
  {
    public ThreeDaysAgendaPageViewModel(IRepository repository, IMessenger messenger,
                                        INavigationService navigationService)
      : base(repository, messenger, navigationService)
    {
      MessengerInstance.Register(this, (InitDayPageMessage m) => SheetDate = m.Date);
    }

    protected override void SetInitialDate()
    {
      SheetDate = DateTime.Today;
    }

    protected override int DayCount
    {
      get { return 3; }
    }

    protected override void FlipSheetBack()
    {
      SheetDate = SheetDate.AddDays(-1);
    }

    protected override void FlipSheetForward()
    {
      SheetDate = SheetDate.AddDays(1);
    }
  }
}