﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.Helpers;
using KarambaPlanning.Repository;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.ViewModel.PageViewModels
{
  /// <summary>
  ///   ViewModel for a day view
  /// </summary>
  public class WeekPageViewModel : AgendaPageViewModel
  {
    public WeekPageViewModel(IRepository repository, IMessenger messenger, INavigationService navigationService)
      : base(repository, messenger, navigationService)
    {
    }

    protected override int DayCount
    {
      get { return 7; }
    }

    protected override void SetInitialDate()
    {
      SheetDate = DateTime.Today.LastDayOfWeek(Culture.FirstDayOfWeek);
    }

    protected override IEnumerable<DateTask> FilterDateTasks(IEnumerable<DateTask> dateTasks)
    {
      return dateTasks.Take(3);
    }

    protected override void FlipSheetBack()
    {
      SheetDate = SheetDate.AddDays(-7);
    }

    protected override void FlipSheetForward()
    {
      SheetDate = SheetDate.AddDays(7);
    }
  }
}