﻿using System.Collections.Generic;
using GalaSoft.MvvmLight;

namespace KarambaPlanning.App1.ViewModel.Print
{
  public class AgendaPrintPageViewModel : ViewModelBase  
  {
    public string DateHeader { get; set; }
    public IEnumerable<AgendumPrintPageViewModel> Tasks { get; set; }
  }
}