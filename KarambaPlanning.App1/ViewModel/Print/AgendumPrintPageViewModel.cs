﻿using GalaSoft.MvvmLight;

namespace KarambaPlanning.App1.ViewModel.Print
{
  public class AgendumPrintPageViewModel : ViewModelBase
  {
    public string TimeHeader { get; set; }
    public string Task { get; set; }
  }
}