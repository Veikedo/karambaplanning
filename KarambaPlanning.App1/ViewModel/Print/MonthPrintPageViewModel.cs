﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using GalaSoft.MvvmLight;
using KarambaPlanning.Helpers;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.ViewModel.Print
{
  public class MonthPrintPageViewModel : ViewModelBase
  {
    private readonly IRepository _repository;
    private readonly ResourceLoader _resources;

    private MonthPrintPageViewModel(IRepository repository)
    {
      _repository = repository;
      _resources = new ResourceLoader();
    }

    public IList<AgendaPrintPageViewModel> DayAgenda { get; set; }

    public static async Task<MonthPrintPageViewModel> CreateAsync(IRepository repository, int year, int month)
    {
      var retValue = new MonthPrintPageViewModel(repository);
      await retValue.Init(year, month).ConfigureAwait(false);
      return retValue;
    }

    private async Task Init(int year, int month)
    {
      DayAgenda = new List<AgendaPrintPageViewModel>();
      var day = new DateTime(year, month, 1);

      int daysInMonth = DateTime.DaysInMonth(year, month);
      var monthTasks = (await _repository.GetDateTasksAsync()).Where(x => x.Date.Month == month).ToList();

      for (int i = 0; i < daysInMonth; i++)
      {
        DateTime t = day;
        var tasks = monthTasks.Where(x => x.Date == t)
                              .OrderBy(x => !x.IsAllDayTask).ThenBy(x => x.StartTime)
                              .ToList();

        if (tasks.Any())
        {
          var model = new AgendaPrintPageViewModel
          {
            DateHeader = day.ToString("D"),
            Tasks = tasks.Select(x => new AgendumPrintPageViewModel
            {
              Task = x.Subject,
              TimeHeader = x.IsAllDayTask ? _resources.GetString("AllDay") : @"{0:hh\:mm} - {1:hh\:mm}".FormatWith(TimeSpan.FromTicks(x.StartTime),
              TimeSpan.FromTicks(x.EndTime))
            })
          };

          DayAgenda.Add(model);
        }

        day = day.AddDays(1);
      }
    }
  }
}