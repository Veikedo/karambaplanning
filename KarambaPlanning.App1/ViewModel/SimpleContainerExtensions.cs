﻿using Caliburn.Micro;

namespace KarambaPlanning.App1.ViewModel
{
  public static class SimpleContainerExtensions
  {
    public static T GetInstance<T>(this SimpleContainer container)
    {
      return (T) container.GetInstance(typeof (T), null);
    }
  }
}