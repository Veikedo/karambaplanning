﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;

namespace KarambaPlanning.App1.ViewModel.Spans
{
  /// <summary>
  ///   Represents a span (term) for interim task
  /// </summary>
  public class InterimTaskSpan : SheetSpanViewModel
  {
    public InterimTaskSpan(IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
      Duration = "12:00 - 14:59";
    }

    /// <summary>
    ///   Time of task's start
    /// </summary>
    public TimeSpan StartTime { get; set; }

    /// <summary>
    ///   Time of task's end
    /// </summary>
    public TimeSpan EndTime { get; set; }

    /// <summary>
    ///   Duration of the task
    /// </summary>
    public string Duration { get; set; }

    /// <summary>
    ///   Creates new interim task
    /// </summary>
    public ICommand CreateNewTaskCommand
    {
      get
      {
        var model = new TaskViewModel(MessengerInstance, NavigationService)
        {
          Date = Date,
          StartTime = StartTime,
          EndTime = EndTime
        };

        return new RelayCommand(() => NavigateToTaskEditPage(model));
      }
    }
  }
}