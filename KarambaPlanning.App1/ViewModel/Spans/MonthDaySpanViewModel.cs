﻿using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace KarambaPlanning.App1.ViewModel.Spans
{
  /// <summary>
  /// Represents a "day box" in month view
  /// </summary>
  public class MonthDaySpanViewModel : SheetSpanViewModel
  {
    public MonthDaySpanViewModel(IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
    }

    /// <summary>
    /// If this day is a day from a previous month
    /// </summary>
    public bool IsCurrentMonthAffix { get; set; }


    /// <summary>
    /// Command that creates new all day task
    /// </summary>
    public ICommand CreateNewTaskCommand
    {
      get
      {
        return new RelayCommand(() => NavigateToTaskEditPage(new TaskViewModel(MessengerInstance, NavigationService)
        {
          Date = Date,
          IsAllDayTask = true
        }));
      }
    }
  }
}