﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace KarambaPlanning.App1.ViewModel.Spans
{
  /// <summary>
  /// Base view model for the time spans 
  /// </summary>
  public class SheetSpanViewModel : BaseViewModel
  {
    public SheetSpanViewModel(IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
    }

    /// <summary>
    /// Time of this span
    /// </summary>
    public DateTime Date { get; set; }

    /// <summary>
    /// Tasks in this span
    /// </summary>
    public IEnumerable<TaskViewModel> Tasks { get; set; }

    /// <summary>
    /// Task count (without filtering)
    /// </summary>
    public int RealTaskCount { get; set; }

    /// <summary>
    /// Opens day page on current date
    /// </summary>
    public ICommand OpenDayPageWithCurrentDateCommand
    {
      get { return new RelayCommand(() => NavigateToDayPage(Date)); }
    }
  }
}