﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using RelayCommand = GalaSoft.MvvmLight.Command.RelayCommand;

namespace KarambaPlanning.App1.ViewModel
{
  /// <summary>
  /// ViewModel for a DateTask
  /// </summary>
  public class TaskViewModel : BaseViewModel
  {
    public TaskViewModel(IMessenger messenger, INavigationService navigationService)
      : base(messenger, navigationService)
    {
    }


    public int Id { get; set; }
    public string Subject { get; set; }
    public string Message { get; set; }
    public string Location { get; set; }
    public bool IsAllDayTask { get; set; }
    public DateTime Date { get; set; }
    public TimeSpan StartTime { get; set; }
    public TimeSpan EndTime { get; set; }

    /// <summary>
    /// Opens edit page to edit underlying DateTask
    /// </summary>
    public ICommand EditTaskCommand
    {
      get { return new RelayCommand(() => NavigateToTaskEditPage(model: this)); }
    }
  }
}