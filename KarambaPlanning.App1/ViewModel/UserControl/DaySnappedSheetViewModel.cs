﻿using System;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.ViewModel.PageViewModels;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.ViewModel.UserControl
{
  /// <summary>
  /// ViewModel for snapped view
  /// </summary>
  public class DaySnappedSheetViewModel : AgendaPageViewModel
  {
    public DaySnappedSheetViewModel(IRepository repository, IMessenger messenger, INavigationService navigationService)
      : base(repository, messenger, navigationService)
    {
    }

    protected override int DayCount
    {
      get { return 1; }
    }

    protected override void FlipSheetBack()
    {
      SheetDate = SheetDate.AddDays(-1);
    }

    protected override void FlipSheetForward()
    {
      SheetDate = SheetDate.AddDays(1);
    }

    protected override void SetInitialDate()
    {
      SheetDate = DateTime.Today;
    }
  }
}