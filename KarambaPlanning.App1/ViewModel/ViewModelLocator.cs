﻿using Caliburn.Micro;
using GalaSoft.MvvmLight.Messaging;
using KarambaPlanning.App1.Common;
using KarambaPlanning.App1.ViewModel.ContractViewModels;
using KarambaPlanning.App1.ViewModel.PageViewModels;
using KarambaPlanning.App1.ViewModel.UserControl;
using KarambaPlanning.Repository.Contracts;
using KarambaPlanning.Repository.Repository;

namespace KarambaPlanning.App1.ViewModel
{
  public class ViewModelLocator
  {
    public static readonly SimpleContainer Container = new SimpleContainer();

    static ViewModelLocator()
    {
      Container.RegisterInstance(typeof (IRepository), null, SqliteRepository.CreateAsync().Result);
      Container.RegisterSingleton(typeof (INavigationService), null, typeof (NavigationService));
      Container.RegisterSingleton(typeof (IMessenger), null, typeof (Messenger));

      Container.RegisterSingleton(typeof (MonthPageViewModel), null, typeof (MonthPageViewModel));
      Container.RegisterSingleton(typeof (WeekPageViewModel), null, typeof (WeekPageViewModel));
      Container.RegisterSingleton(typeof (ThreeDaysAgendaPageViewModel), null, typeof (ThreeDaysAgendaPageViewModel));
      Container.RegisterSingleton(typeof (TaskEditPageViewModel), null, typeof (TaskEditPageViewModel));
      Container.RegisterSingleton(typeof (DaySnappedSheetViewModel), null, typeof (DaySnappedSheetViewModel));

      Container.RegisterSingleton(typeof (SettingsPaneViewModel), null, typeof (SettingsPaneViewModel));
      
      // hacks
      Container.GetInstance<TaskEditPageViewModel>();
      Container.GetInstance<ThreeDaysAgendaPageViewModel>();
      Container.GetInstance<WeekPageViewModel>();
      Container.GetInstance<SettingsPaneViewModel>();
    }

    public MonthPageViewModel MonthPage
    {
      get { return Container.GetInstance<MonthPageViewModel>(); }
    }

    public TaskEditPageViewModel TaskEditPage
    {
      get { return Container.GetInstance<TaskEditPageViewModel>(); }
    }

    public WeekPageViewModel WeekPage
    {
      get { return Container.GetInstance<WeekPageViewModel>(); }
    }

    public AgendaPageViewModel ThreeDaysPage
    {
      get { return Container.GetInstance<ThreeDaysAgendaPageViewModel>(); }
    }

    public DaySnappedSheetViewModel DaySnappedSheet
    {
      get { return Container.GetInstance<DaySnappedSheetViewModel>(); }
    }

    public SettingsPaneViewModel SettingsPane
    {
      get { return Container.GetInstance<SettingsPaneViewModel>(); }
    }

    public static void Cleanup()
    {
    }
  }
}