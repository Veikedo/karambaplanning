﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Graphics.Printing;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Printing;
using KarambaPlanning.App1.ViewModel;
using KarambaPlanning.App1.ViewModel.PageViewModels;
using KarambaPlanning.App1.ViewModel.Print;
using KarambaPlanning.App1.Views.Print;
using KarambaPlanning.Repository.Contracts;

namespace KarambaPlanning.App1.Views
{
  public sealed partial class MonthSheetPage
  {
    private readonly List<UIElement> _printPages;
    private Task _addPrintPagesTask;
    private PrintDocument _printDocument;
    private IPrintDocumentSource _printDocumentSource;

    public MonthSheetPage()
    {
      InitializeComponent();
      _printPages = new List<UIElement>();
    }

    public DateTime CurrentSheetDate
    {
      get { return ViewModelLocator.Container.GetInstance<MonthPageViewModel>().SheetDate; }
    }

    private void GridOnSizeChanged(object sender, SizeChangedEventArgs e)
    {
      Calendar.Width = e.NewSize.Width;
      Calendar.Height = e.NewSize.Height;
    }

    protected override void OnNavigatedTo(NavigationEventArgs e)
    {
      base.OnNavigatedTo(e);
      RegisterForPrinting();
    }

    protected override void OnNavigatedFrom(NavigationEventArgs e)
    {
      base.OnNavigatedFrom(e);
      UnregisterForPrinting();
    }

    #region Printing

    private void RegisterForPrinting()
    {
      _printDocument = new PrintDocument();
      _printDocumentSource = _printDocument.DocumentSource;
      _printDocument.Paginate += CreatePrintPreviewPages;
      _printDocument.GetPreviewPage += GetPrintPreviewPage;
      _printDocument.AddPages += AddPrintPages;
      PrintManager printMan = PrintManager.GetForCurrentView();
      printMan.PrintTaskRequested += PrintTaskRequested;
    }

    private void UnregisterForPrinting()
    {
      if (_printDocument != null)
      {
        _printDocument.Paginate -= CreatePrintPreviewPages;
        _printDocument.GetPreviewPage -= GetPrintPreviewPage;
        _printDocument.AddPages -= AddPrintPages;
        PrintManager printMan = PrintManager.GetForCurrentView();
        printMan.PrintTaskRequested -= PrintTaskRequested;
      }
    }

    private async void CreatePrintPreviewPages(object sender, PaginateEventArgs e)
    {
      _printPages.Clear();
      PrintingRoot.Children.Clear();

      var printingOptions = e.PrintTaskOptions;
      var pageDescription = printingOptions.GetPageDescription(0);

      _addPrintPagesTask = AddPrintPagesAsync(pageDescription);
      await _addPrintPagesTask;

      ((PrintDocument) sender).SetPreviewPageCount(_printPages.Count, PreviewPageCountType.Intermediate);
    }

    private async Task AddPrintPagesAsync(PrintPageDescription pageDescription)
    {
      var repository = ViewModelLocator.Container.GetInstance<IRepository>();
      MonthPrintPageViewModel model = await MonthPrintPageViewModel.CreateAsync(repository, CurrentSheetDate.Year,
                                                                                CurrentSheetDate.Month);

      var printPage = new PrintPage
      {
        DataContext = model,
        Width = pageDescription.PageSize.Width,
        Height = pageDescription.PageSize.Height
      };

      UIElement element = null;
      RichTextBlockOverflow rtbo = printPage.Link;

      do
      {
        if (element == null)
        {
          element = printPage;
        }
        else
        {
          var continuationPage = new ContinuationPage
          {
            Width = pageDescription.PageSize.Width,
            Height = pageDescription.PageSize.Height
          };

          element = continuationPage;
          rtbo.OverflowContentTarget = continuationPage.Link;
          rtbo = continuationPage.Link;
        }

        PrintingRoot.Children.Add(element);
        PrintingRoot.InvalidateMeasure();
        PrintingRoot.UpdateLayout();

        _printPages.Add(element);
      } while (rtbo.HasOverflowContent);
    }

    private async void GetPrintPreviewPage(object sender, GetPreviewPageEventArgs e)
    {
      await _addPrintPagesTask;
      ((PrintDocument) sender).SetPreviewPage(e.PageNumber, _printPages[e.PageNumber - 1]);
    }

    private async void AddPrintPages(object sender, AddPagesEventArgs e)
    {
      await _addPrintPagesTask;
      foreach (UIElement element in _printPages)
      {
        _printDocument.AddPage(element);
      }

      ((PrintDocument) sender).AddPagesComplete();
    }

    private void PrintTaskRequested(PrintManager sender, PrintTaskRequestedEventArgs e)
    {
      PrintTask printTask = null;
      printTask = e.Request.CreatePrintTask("Month tasks", sourceRequested =>
      {
        printTask.Completed += async (s, args) =>
        {
          if (args.Completion == PrintTaskCompletion.Failed)
          {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
              var dialog = new MessageDialog("Something went wrong while trying to print. Please try again.");
              await dialog.ShowAsync();
            });
          }
        };

        sourceRequested.SetSource(_printDocumentSource);
      });
    }

    #endregion
  }
}