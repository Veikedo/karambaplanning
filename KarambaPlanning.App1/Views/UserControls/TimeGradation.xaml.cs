﻿using System.Linq;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace KarambaPlanning.App1.Views.UserControls
{
  public sealed partial class TimeGradation : UserControl
  {
    public TimeGradation()
    {
      InitializeComponent();
      Numbers.ItemsSource = Enumerable.Range(0, 24);
    }
  }
}