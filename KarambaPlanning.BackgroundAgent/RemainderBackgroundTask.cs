﻿using System;
using System.Linq;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.Resources;
using Windows.Data.Xml.Dom;
using Windows.Storage;
using Windows.UI.Notifications;
using KarambaPlanning.Repository.Contracts;
using KarambaPlanning.Repository.Repository;

namespace KarambaPlanning.BackgroundAgent
{
  public sealed class RemainderBackgroundTask : IBackgroundTask
  {
    private static readonly IRepository Repository;
    private readonly long _notifyTime;

    static RemainderBackgroundTask()
    {
      Repository = SqliteRepository.CreateAsync().Result;
    }

    public RemainderBackgroundTask()
    {
      _notifyTime = (long) ApplicationData.Current.LocalSettings.Values["Remainder"];
    }

    public async void Run(IBackgroundTaskInstance taskInstance)
    {
      BackgroundTaskDeferral deferral = taskInstance.GetDeferral();
      var resources = new ResourceLoader();

      try
      {
        var currentDate = DateTime.Today;
        long currentTime = DateTime.Now.TimeOfDay.Ticks;

        var todayTasks = await Repository.GetDateTasksAsync(x => x.IsAllDayTask == false && x.Date == currentDate &&
                                                                 x.StartTime >= currentTime);
        var any = todayTasks.Any(x => x.StartTime - currentTime <= _notifyTime);

        if (any)
        {
          // ReSharper disable once ResourceItemNotResolved
          var xml = @"<toast launch=""toast""> 
                        <visual> 
                          <binding template=""ToastText01""> 
                            <text id=""1"">" + resources.GetString("remind") + @"</text> 
                          </binding> 
                        </visual> 
                      </toast>";

          var xmlDocument = new XmlDocument();
          xmlDocument.LoadXml(xml);

          var toast = new ToastNotification(xmlDocument);
          ToastNotifier notifier = ToastNotificationManager.CreateToastNotifier();
          notifier.Show(toast);
        }
      }
      catch
      {
      }
      finally
      {
        deferral.Complete();
      }
    }
  }
}