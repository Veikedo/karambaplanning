﻿using System;

namespace KarambaPlanning.Helpers
{
  public static class DateTimeExtensions
  {

    /// <summary>
    /// Returns last date of specified day of week
    /// </summary>
    public static DateTime LastDayOfWeek(this DateTime date, DayOfWeek dayOfWeek)
    {
      DateTime targetDate = date.AddDays(-1);
      while (targetDate.DayOfWeek != dayOfWeek)
      {
        targetDate = targetDate.AddDays(-1);
      }

      return targetDate;
    }

    /// <summary>
    /// Returns next date of specified day of week
    /// </summary>

    public static DateTime NextDayOfWeek(this DateTime date, DayOfWeek dayOfWeek)
    {
      DateTime targetDate = date.AddDays(1);
      while (targetDate.DayOfWeek != dayOfWeek)
      {
        targetDate = targetDate.AddDays(1);
      }

      return targetDate;
    }

    /// <summary>
    /// Returns date of first day of month of specified date
    /// </summary>
    public static DateTime FirstDayOfMonth(this DateTime date)
    {
      return new DateTime(date.Year, date.Month, 1);
    }

    /// <summary>
    /// Returns date of last day of month of specified date
    /// </summary>
    public static DateTime LastDayOfMonth(this DateTime date)
    {
      return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
    }

    /// <summary>
    /// Returns fist date of specified day of week of passed date
    /// </summary>
    public static DateTime FirstDayOfWeekOfMonth(this DateTime date, DayOfWeek dayOfWeek)
    {
      var firstDayOfMonth = date.FirstDayOfMonth();
      return firstDayOfMonth.DayOfWeek == dayOfWeek ? firstDayOfMonth : firstDayOfMonth.NextDayOfWeek(dayOfWeek);
    }
  }
}