using System;
using System.Collections.Generic;
using System.Globalization;

namespace KarambaPlanning.Helpers
{
  /// <summary>
  ///   Culture specific DayOfWeek comparer
  /// </summary>
  public class DayOfWeekComparer : IComparer<DayOfWeek>
  {
    private readonly DayOfWeek[] _sortedWeek = new DayOfWeek[7];

    public DayOfWeekComparer()
    {
      SortWeek();
    }

    private bool IsCurrentWeekOrdered
    {
      get { return _sortedWeek[0] == FirstDayOfWeek; }
    }

    private static DayOfWeek FirstDayOfWeek
    {
      get { return DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek; }
    }

    public int Compare(DayOfWeek x, DayOfWeek y)
    {
      if (!IsCurrentWeekOrdered)
      {
        SortWeek();
      }

      return Array.IndexOf(_sortedWeek, x) - Array.IndexOf(_sortedWeek, y);
    }

    private void SortWeek()
    {
      DayOfWeek day = FirstDayOfWeek;

      for (int i = 0; i < 7; i++)
      {
        _sortedWeek[i] = (DayOfWeek) ((int) (day + i)%7);
      }
    }
  }
}