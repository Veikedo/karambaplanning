﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KarambaPlanning.Helpers
{
  public static class TimeSpanExtensions
  {
    public static TimeSpan AddHours(this TimeSpan timeSpan, int hours)
    {
      return timeSpan.Add(TimeSpan.FromHours(hours));
    }
  }
}
