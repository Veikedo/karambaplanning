﻿using System;
using SQLite;

namespace KarambaPlanning.Models
{
  /// <summary>
  ///   Represents date task
  /// </summary>
  public class DateTask
  {
    /// <summary>
    ///   Task id
    /// </summary>
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }

    /// <summary>
    ///   Subject of task
    /// </summary>
    public string Subject { get; set; }

    /// <summary>
    ///   Message of task
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    ///   Location of task
    /// </summary>
    public string Location { get; set; }

    /// <summary>
    ///   Is task all day
    /// </summary>
    public bool IsAllDayTask { get; set; }

    /// <summary>
    ///   Date of task
    /// </summary>
    public DateTime Date { get; set; }

    /// <summary>
    ///   StartTime of task
    /// </summary>
    public long StartTime { get; set; }

    /// <summary>
    ///   EndTime of task
    /// </summary>
    public long EndTime { get; set; }
  }
}