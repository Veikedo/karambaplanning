﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace KarambaPlanning.Repository.Contracts
{
  public interface IRepository
  {
    Task<IEnumerable<DateTask>> GetDateTasksAsync();
    Task<IEnumerable<DateTask>> GetDateTasksAsync(Expression<Func<DateTask, bool>> pred);
    Task<bool> CreateDateTaskAsync(DateTask instance);
    Task<bool> UpdateDateTaskAsync(DateTask instance);
    Task<bool> RemoveDateTaskAsync(int dayTaskId);
    Task RemoveAllDateTasksAsync();
    Task<int> CountAsync();
    Task<int> CountAsync(Expression<Func<DateTask, bool>> pred);
  }
}