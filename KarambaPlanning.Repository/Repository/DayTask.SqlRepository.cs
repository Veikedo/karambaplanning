﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KarambaPlanning.Models;
using SQLite;

namespace KarambaPlanning.Repository.Repository
{
  public partial class SqliteRepository
  {
    #region DayTaskRepo

    private AsyncTableQuery<DayTask> DayTasksTable
    {
      get { return _db.Table<DayTask>(); }
    }

    public async Task<IEnumerable<DayTask>> GetDayTasksAsync()
    {
      return await DayTasksTable.ToListAsync();
    }

    public async Task<IEnumerable<DayTask>> GetDayTasksAsync(Func<DayTask, bool> pred)
    {
      return (await DayTasksTable.ToListAsync()).Where(pred);
    }

    public async Task<bool> CreateDayTaskAsync(DayTask instance)
    {
      return await InsertAsync(instance);
    }

    public async Task<bool> UpdateDayTaskAsync(DayTask instance)
    {
      return await UpdateAsync(instance, DayTasksTable);
    }

    public async Task<bool> RemoveDayTaskAsync(int dayTaskId)
    {
      return await RemoveAsync(dayTaskId, DayTasksTable);
    }

    #endregion
  }
}