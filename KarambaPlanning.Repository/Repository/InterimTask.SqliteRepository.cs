﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KarambaPlanning.Models;
using SQLite;

namespace KarambaPlanning.Repository.Repository
{
  public partial class SqliteRepository
  {
    private AsyncTableQuery<InterimTask> InterimTasksTable
    {
      get { return _db.Table<InterimTask>(); }
    }

    public async Task<IEnumerable<InterimTask>> GetInterimTasksAsync()
    {
      return await InterimTasksTable.ToListAsync();
    }

    public async Task<IEnumerable<InterimTask>> GetInterimTasksAsync(Func<InterimTask, bool> pred)
    {
      return (await InterimTasksTable.ToListAsync()).Where(pred);
    }

    public async Task<bool> CreateInterimTask(InterimTask instance)
    {
      return await InsertAsync(instance);
    }

    public async Task<bool> UpdateInterimTaskAsync(InterimTask instance)
    {
      return await UpdateAsync(instance, InterimTasksTable);
    }

    public async Task<bool> RemoveInterimTaskAsync(int interimTaskId)
    {
      return await RemoveAsync(interimTaskId, InterimTasksTable);
    }
  }
}