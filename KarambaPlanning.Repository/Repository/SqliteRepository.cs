﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Windows.Storage;
using KarambaPlanning.Repository.Contracts;
using SQLite;

namespace KarambaPlanning.Repository.Repository
{
  public class SqliteRepository : IRepository
  {
    private const string DatabaseName = "calendar.db";
    private SQLiteAsyncConnection _db;

    private SqliteRepository()
    {
    }

    [Pure]
    public static async Task<SqliteRepository> CreateAsync()
    {
      var instanse = new SqliteRepository();
      bool dbNotInit = true;

      try
      {
        await ApplicationData.Current.LocalFolder.GetFileAsync(DatabaseName);
        dbNotInit = false;
      }
      catch (FileNotFoundException)
      {
      }

      instanse._db = new SQLiteAsyncConnection(DatabaseName);

      if (dbNotInit)
      {
        await instanse._db.CreateTableAsync<DateTask>().ConfigureAwait(false);
      }

      return instanse;
    }

    #region DateTask

    private AsyncTableQuery<DateTask> DateTasksTable
    {
      get { return _db.Table<DateTask>(); }
    }

    public async Task<bool> RemoveDateTaskAsync(int idDateTask)
    {
      var cache = await DateTasksTable.Where(x => x.Id == idDateTask).FirstOrDefaultAsync();

      if (cache != null)
      {
        return await _db.DeleteAsync(cache) > 0;
      }

      return false;
    }

    public async Task RemoveAllDateTasksAsync()
    {
      foreach (var task in await DateTasksTable.ToListAsync())
      {
        await _db.DeleteAsync(task);
      }
    }

    public async Task<IEnumerable<DateTask>> GetDateTasksAsync()
    {
      return await DateTasksTable.ToListAsync().ConfigureAwait(false);
    }

    public async Task<IEnumerable<DateTask>> GetDateTasksAsync(Expression<Func<DateTask, bool>> pred)
    {
      return await DateTasksTable.Where(pred).ToListAsync().ConfigureAwait(false);
    }

    public async Task<bool> CreateDateTaskAsync(DateTask instance)
    {
      return await _db.InsertAsync(instance) > 0;
    }

    public async Task<bool> UpdateDateTaskAsync(DateTask instance)
    {
      var cache = await DateTasksTable.Where(x => x.Id == instance.Id).FirstOrDefaultAsync();

      if (cache != null)
      {
        return await _db.UpdateAsync(instance) > 0;
      }

      return false;
    }

    public async Task<int> CountAsync()
    {
      return await DateTasksTable.CountAsync();
    }

    public async Task<int> CountAsync(Expression<Func<DateTask, bool>> pred)
    {
      return await DateTasksTable.Where(pred).CountAsync();
    }

    #endregion
  }
}