﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using KarambaPlanning.Models;
using SQLite;

namespace KarambaPlanning.Repository.Repository
{
  public partial class SqliteRepository
  {
    private AsyncTableQuery<TaskBody> TaskBodiesTable
    {
      get { return _db.Table<TaskBody>(); }
    }

    public async Task<bool> RemoveTaskBodyAsync(int idTaskBody)
    {
      return await RemoveAsync(idTaskBody, TaskBodiesTable);
    }

    public async Task<IEnumerable<TaskBody>> GetTaskBodiesAsync()
    {
      return await TaskBodiesTable.ToListAsync();
    }

    public async Task<IEnumerable<TaskBody>> GetTaskBodiesAsync(Func<TaskBody, bool> pred)
    {
      return (await TaskBodiesTable.ToListAsync()).Where(pred);
    }

    public async Task<bool> CreateTaskBodyAsync(TaskBody instance)
    {
      return await InsertAsync(instance);
    }

    public async Task<bool> UpdateTaskBodyAsync(TaskBody instance)
    {
      return await UpdateAsync(instance, TaskBodiesTable);
    }
  }
}